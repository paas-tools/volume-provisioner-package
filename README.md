# Volume provisioner package

This Go package can be used to easily implement a Volume Provisioner controller.

# Usage

To use it, simply satisfy the `VolumeProvisioner` [interface](volumeprosioner.go#L40) and call the `StartVolumeProvisoner` function with your arguments of choice
