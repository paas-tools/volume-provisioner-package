/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package volumeprovisioner

import (
	"fmt"
	"time"

	"github.com/golang/glog"

	"k8s.io/api/core/v1"
	storage_v1 "k8s.io/api/storage/v1"
	meta_v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/util/workqueue"
)

// Controller is a struct with all the objects needed to watch a given Kubernetes resource
type Controller struct {
	indexer  cache.Indexer
	queue    workqueue.RateLimitingInterface
	informer cache.Controller
	client   kubernetes.Interface
	vp       VolumeProvisioner
	dryRun   bool
}

// NewController is the builder Controller function
func NewController(queue workqueue.RateLimitingInterface, indexer cache.Indexer, informer cache.Controller, client kubernetes.Interface, vp VolumeProvisioner, dryRun bool) *Controller {
	return &Controller{
		informer: informer,
		indexer:  indexer,
		queue:    queue,
		client:   client,
		vp:       vp,
		dryRun:   dryRun,
	}
}

func (c *Controller) processNextItem() bool {
	// Wait until there is a new item in the working queue
	key, quit := c.queue.Get()
	if quit {
		return false
	}
	// Tell the queue that we are done with processing this key. This unblocks the key for other workers
	// This allows safe parallel processing because two objects with the same key are never processed in
	// parallel.
	defer c.queue.Done(key)

	// Invoke the method containing the business logic
	err := c.doWork(key.(string))
	// Handle the error if something went wrong during the execution of the business logic
	c.handleErr(err, key)
	return true
}

// doWork is the business logic of the controller. In case an error happened, it has to simply return the error.
// The retry logic should not be part of the business logic.
func (c *Controller) doWork(key string) error {
	obj, exists, err := c.indexer.GetByKey(key)
	if err != nil {
		glog.Errorf("Fetching object with key %s from store failed with %v", key, err)
		return err
	}

	if !exists {
		glog.Infof("Object %s does not exist anymore\n", key)
		return nil
	}

	pvc := obj.(*v1.PersistentVolumeClaim)
	glog.V(2).Infof("- Sync/Add/Update for PVC %s/%s\n", pvc.GetNamespace(), pvc.GetName())

	// Only process PVC for this provisioner
	if pvcProvisioner := pvc.GetAnnotations()[kubernetesStorageProvisionerAnnotation]; pvcProvisioner == c.vp.GetProvisionerName() {
		glog.V(2).Infof("PVC '%s/%s' provisioner '%s', matches this provisioner, proceeding ...\n",
			pvc.GetNamespace(), pvc.GetName(), pvcProvisioner)
		// Only process PVC that are Pending, ignore the rest
		if pvc.Status.Phase == v1.ClaimPending && pvc.Spec.VolumeName == "" {
			glog.Infof("PVC '%s/%s' is in state %s, proceeding ...\n", pvc.GetNamespace(), pvc.GetName(), pvc.Status.Phase)
			// Obtain StorageClass associated to this PVC
			storageClass, err := c.getStorageClass(pvc)
			if err != nil {
				return err
			}
			glog.V(2).Infof("PVC '%s/%s' will be processed ...\n", pvc.GetNamespace(), pvc.GetName())
			pv, err := c.vp.CreatePV(pvc, storageClass)
			if err != nil {
				return err
			}

			// Set custom annotations in PV with info of storageclass and pvc creation
			setCustomMetadata(pv, *pvc, *storageClass)

			if !c.dryRun {
				// Save PV in OpenShift
				if err := c.savePV(pv); err != nil {
					return err
				}
			} else {
				glog.Infoln("Dry-run enabled: object not changed")
			}

		} else {
			glog.V(2).Infof("PVC '%s/%s' is in state %s, ignoring ...\n", pvc.GetNamespace(), pvc.GetName(), pvc.Status.Phase)
		}
	} else {
		glog.V(2).Infof("PVC '%s/%s' provisioner does not match '%s', ignoring ...\n", pvc.GetNamespace(), pvc.GetName(), c.vp.GetProvisionerName())
	}
	return nil
}

// savePV persists the PersistentVolume definition in OpenShift. The operation is
// retried multiple times in case saving it fails for some reason. In case the PV is never created,
// the resources are cleaned up.
func (c *Controller) savePV(pv *v1.PersistentVolume) error {
	// Obtain a client to be able to modify PVs
	pvClient := c.client.CoreV1().PersistentVolumes()

	for i := 0; i < createProvisionedPVRetryCount; i++ {
		result, pverror := pvClient.Create(pv)
		if pverror == nil {
			// PV successfully created
			glog.Infof("PV '%s' successfully created for PVC %s/%s!\n",
				result.GetObjectMeta().GetName(), pv.Annotations["cern.ch/created-for-pvc-namespace"],
				pv.Annotations["cern.ch/created-for-pvc-name"])
			return nil
		}
		// PV failed to be created
		glog.Errorf("Failed to persist PV for PVC '%s/%s': %v\n", pv.Annotations["cern.ch/created-for-pvc-namespace"],
			pv.Annotations["cern.ch/created-for-pvc-name"], pverror)
		if i+1 < createProvisionedPVRetryCount {
			// Retry if there are still more attempts...
			glog.Errorln("Retrying...")
			time.Sleep(createProvisionedPVInterval)
		}

	}
	glog.Errorf("PV failed to be created after several attempts, unprovisioning volume...\n")
	if err := c.vp.UnprovisionPV(pv); err != nil {
		// If there is another error unprovisioning the volume just return the new error
		return err
	}
	return fmt.Errorf("Failure to persist the PV created for PVC %s/%s. Physical volumes has "+
		"been successfully unprovisioned so there are no resources left behind. PVC will be retried. PV definition was: %v",
		pv.Annotations["cern.ch/created-for-pvc-namespace"], pv.Annotations["cern.ch/created-for-pvc-name"], pv)
}

// getStorageClass obtains the associated storageClass for the v1.PersistentVolumeClaim
// passed as argument. Please check the StorageClass spec field and if not present,
// use beta annotation. If none are present raise an error
func (c *Controller) getStorageClass(pvc *v1.PersistentVolumeClaim) (*storage_v1.StorageClass, error) {
	// Obtain the name of the storageClass from the PVC
	storageClassName := GetStorageClassName(pvc)
	if storageClassName == nil {
		return nil, fmt.Errorf("PVC '%s/%s' has not storageclass, aborting... ", pvc.GetNamespace(), pvc.GetName())
	}
	// Retrieve the StorageClass object from the name
	storageClass, err := c.client.StorageV1().StorageClasses().Get(*storageClassName, meta_v1.GetOptions{})
	if err != nil {
		glog.Errorf("PVC '%s/%s' does not have a valid storageClass. Ignoring...", pvc.GetNamespace(), pvc.GetName())
		return nil, err
	}
	glog.V(2).Infof("StorageClass in use: '%s'\n", storageClass.GetName())
	return storageClass, nil
}

// handleErr checks if an error happened and makes sure we will retry later.
func (c *Controller) handleErr(err error, key interface{}) {
	if err == nil {
		// Forget about the #AddRateLimited history of the key on every successful synchronization.
		// This ensures that future processing of updates for this key is not delayed because of
		// an outdated error history.
		c.queue.Forget(key)
		return
	}

	// This controller retries 5 times if something goes wrong. After that, it stops trying.
	if c.queue.NumRequeues(key) < 5 {
		glog.Infof("Error syncing object %v: %v", key, err)

		// Re-enqueue the key rate limited. Based on the rate limiter on the
		// queue and the re-enqueue history, the key will be processed later again.
		c.queue.AddRateLimited(key)
		return
	}

	c.queue.Forget(key)
	// Report to an external entity that, even after several retries, we could not successfully process this key
	runtime.HandleError(err)
	glog.Infof("Dropping Object %q out of the queue: %v", key, err)
}

// Run starts a watch process for the object queue
func (c *Controller) Run(threadiness int, stopCh chan struct{}) {
	defer runtime.HandleCrash()

	// Let the workers stop when we are done
	defer c.queue.ShutDown()
	glog.Info("Starting Kubernetes controller")

	go c.informer.Run(stopCh)

	// Wait for all involved caches to be synced, before processing items from the queue is started
	if !cache.WaitForCacheSync(stopCh, c.informer.HasSynced) {
		runtime.HandleError(fmt.Errorf("Timed out waiting for caches to sync"))
		return
	}

	for i := 0; i < threadiness; i++ {
		go wait.Until(c.runWorker, time.Second, stopCh)
	}

	<-stopCh
	glog.Info("Stopping Kubernetes controller")
}

func (c *Controller) runWorker() {
	for c.processNextItem() {
	}
}

// StartVolumeProvisoner creates a worqueue to be processed by the controller. The VolumeProvisioner
// passed as argumente will vary depending in the implementation and the  kind of volumes to watch.
func StartVolumeProvisoner(vp VolumeProvisioner, namespace string, clientset *kubernetes.Clientset, dryRun bool) chan struct{} {
	pvcListWatcher := cache.NewListWatchFromClient(clientset.CoreV1().RESTClient(), "persistentVolumeClaims", namespace, fields.Everything())
	if namespace != "" {
		glog.Infof("Watching objects for namespace '%s'\n", namespace)
	} else {
		glog.Infoln("Watching objects for all namespaces")
	}

	if dryRun {
		glog.Infoln("Running in dry-run mode, operations will not change data.")
	}

	// create the workqueue
	queue := workqueue.NewRateLimitingQueue(workqueue.DefaultControllerRateLimiter())

	// Bind the workqueue to a cache with the help of an informer. This way we make sure that
	// whenever the cache is updated, the Object key is added to the workqueue.
	// Note that when we finally process the item from the workqueue, we might see a newer version
	// of the Object than the version which was responsible for triggering the update.
	indexer, informer := cache.NewIndexerInformer(pvcListWatcher, &v1.PersistentVolumeClaim{}, 0, cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(obj)
			if err == nil {
				queue.Add(key)
			}
		},
		UpdateFunc: func(old interface{}, new interface{}) {
			key, err := cache.MetaNamespaceKeyFunc(new)
			if err == nil {
				queue.Add(key)
			}
		},
		DeleteFunc: func(obj interface{}) {
			// IndexerInformer uses a delta queue, therefore for deletes we have to use this
			// key function.
			key, err := cache.DeletionHandlingMetaNamespaceKeyFunc(obj)
			if err == nil {
				queue.Add(key)
			}
		},
	}, cache.Indexers{})

	controller := NewController(queue, indexer, informer, clientset, vp, dryRun)

	// Now let's start the controller
	stop := make(chan struct{})
	go controller.Run(1, stop)
	return stop
}
