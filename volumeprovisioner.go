/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package volumeprovisioner

import (
	"encoding/json"
	"time"

	"k8s.io/api/core/v1"
	storage_v1 "k8s.io/api/storage/v1"
)

// PVC annotations set by Kubernetes volume admission controller
const (
	kubernetesStorageClassAnnotation       = "volume.beta.kubernetes.io/storage-class"
	kubernetesStorageProvisionerAnnotation = "volume.beta.kubernetes.io/storage-provisioner"
)

// Number of retries to create a PV once the physical volume already exists
const (
	createProvisionedPVRetryCount = 10
	createProvisionedPVInterval   = 5 * time.Second
)

// VolumeProvisioner is an interface that represents a single volume provisioner, which takes
// care of provisioning and generating an appropiate *v1.PersistentVolume for the request
type VolumeProvisioner interface {
	CreatePV(*v1.PersistentVolumeClaim, *storage_v1.StorageClass) (*v1.PersistentVolume, error)
	UnprovisionPV(*v1.PersistentVolume) error
	GetProvisionerName() string
}

// GetStorageClassName obtains the name of the storageClass from the PVC definition.
// First check the storageClassName in the `.spec` of the PersistentVolume and if not
// present check the old bena annotation field
// Return nil if there is no storageClass
func GetStorageClassName(pvc *v1.PersistentVolumeClaim) *string {
	if pvc.Spec.StorageClassName != nil {
		return pvc.Spec.StorageClassName
	}
	if storageClassName, ok := pvc.GetAnnotations()[kubernetesStorageClassAnnotation]; ok {
		return &storageClassName
	}
	return nil
}

// setCustomMetadata sets annotation to PersistentVolume based on PVC and StorageClass
func setCustomMetadata(pv *v1.PersistentVolume, pvc v1.PersistentVolumeClaim, storageClass storage_v1.StorageClass) error {
	// Initialize annotations if nil
	if pv.GetAnnotations() == nil {
		pv.Annotations = make(map[string]string)
	}
	pv.Annotations["cern.ch/created-for-pvc-name"] = pvc.GetName()
	pv.Annotations["cern.ch/created-for-pvc-namespace"] = pvc.GetNamespace()
	storageClassParams, err := json.Marshal(storageClass.Parameters)
	if err != nil {
		return err
	}
	pv.Annotations["cern.ch/storage-class-parameters"] = string(storageClassParams)
	return nil
}
